const cars = [];
const button1 = document.querySelector('#agregar');
const agregados = document.querySelector('#mostrar');
const modelos = document.querySelector('input[type="text"]');
const button2 = document.querySelector('#eliminarPri');
const button3 = document.querySelector('#eliminarUlt');

function lista() {
    agregados.innerHTML = "";
    for (element of cars) {
        agregados.innerHTML += element + "<br>";
    }
};

button1.addEventListener('click', function (e) {

    cars.push(modelos.value);
    cars.value = "";
    lista();
});

button2.addEventListener('click', function (e) {
    cars.shift();
    lista();
});

button3.addEventListener('click', function(e) {
    cars.pop();
    lista();
});
